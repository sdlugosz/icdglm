# R package: "icdGLM" #
## Description ##
Provides an estimator for generalized linear models with incomplete data for discrete covariates. The estimation is based on the EM algorithm by the method of weights by Ibrahim (1990) <DOI:10.2307/2290013>.

## Installation ##
### From CRAN ###
The easiest way to use any of the functions in the icdGLM package is to install the CRAN version.  It can be installed from within R using the command:

```
#!R

install.packages("icdGLM")
```


### From bitbucket ###
The devtools package contains functions that allow you to install R packages directly from bitbucket or github. If you've installed and loaded the devtools package, the installation command is

```
#!R

install_bitbucket("sdlugosz/icdGLM")
```
